package org.droidtr.tabgame;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import java.util.*;

public class MainActivity extends Activity 
{
	boolean hile=false;
	String gamesting="";
	String currentstring="";
	int score=0;
	int lvl=1;
	Random r = new Random();
	Handler h = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		final SharedPreferences sp = getSharedPreferences("score",MODE_PRIVATE);
		LinearLayout main= new LinearLayout(this);
		LinearLayout rowone= new LinearLayout(this);
		LinearLayout rowtwo= new LinearLayout(this);
		LinearLayout rowtree= new LinearLayout(this);
		LinearLayout.LayoutParams defparam = new LinearLayout.LayoutParams(-1,-1,1);
		final TextView a =new TextView(this);
		final TextView b =new TextView(this);
		final TextView c =new TextView(this);
		final TextView d =new TextView(this);
		final TextView e =new TextView(this);
		final TextView f =new TextView(this);
		final TextView g =new TextView(this);
		final TextView h =new TextView(this);
		final TextView n =new TextView(this);
		main.setLayoutParams(defparam);
		rowone.setLayoutParams(defparam);
		rowtwo.setLayoutParams(defparam);
		rowtree.setLayoutParams(defparam);
		a.setLayoutParams(defparam);
		b.setLayoutParams(defparam);
		c.setLayoutParams(defparam);
		d.setLayoutParams(defparam);
		e.setLayoutParams(defparam);
		f.setLayoutParams(defparam);
		g.setLayoutParams(defparam);
		h.setLayoutParams(defparam);
		n.setLayoutParams(defparam);
		main.setOrientation(LinearLayout.VERTICAL);
		rowone.setOrientation(LinearLayout.HORIZONTAL);
		rowtwo.setOrientation(LinearLayout.HORIZONTAL);
		rowtree.setOrientation(LinearLayout.HORIZONTAL);
		main.addView(rowone);
		main.addView(rowtwo);
		main.addView(rowtree);
		rowone.addView(a);
		rowone.addView(b);
		rowone.addView(c);
		rowtwo.addView(d);
		rowtwo.addView(e);
		rowtwo.addView(f);
		rowtree.addView(g);
		rowtree.addView(h);
		rowtree.addView(n);
		a.setBackgroundColor(Color.BLUE);
		b.setBackgroundColor(Color.RED);
		c.setBackgroundColor(Color.GREEN);
		d.setBackgroundColor(Color.YELLOW);
		e.setBackgroundColor(Color.CYAN);
		f.setBackgroundColor(Color.MAGENTA);
		g.setBackgroundColor(Color.DKGRAY);
		h.setBackgroundColor(Color.WHITE);
		n.setBackgroundColor(Color.BLACK);
		a.setTag("a");
		b.setTag("b");
		c.setTag("c");
		d.setTag("d");
		e.setTag("e");
		f.setTag("f");
		g.setTag("g");
		h.setTag("h");
		n.setTag("n");
		h.post(new Runnable(){

				@Override
				public void run()
				{
					score=score-1;
					if(score<0){
						score=0;
						
					}
					setTitle("Score:"+score+" Max Score:"+sp.getInt("max",score));
					h.postDelayed(this,1000);
				}
			});
		if(!hile){
		gamenext();
		yak(gamesting,a,b,c,d,e,f,g,h,n);
		}
		OnLongClickListener hileac=new OnLongClickListener(){

			@Override
			public boolean onLongClick(View p1)
			{
				hile=!hile;
				if(!hile){
					System.exit(0);
				}
				gamesting="";
				currentstring="";
				return false;
			}
		};
		e.setOnLongClickListener(hileac);
		final OnClickListener reset=new OnClickListener(){

			@Override
			public void onClick(View p1)
			{
				onCreate(new Bundle());
			}
		};
		OnClickListener ocl = new OnClickListener(){

			@Override
			public void onClick(View p1)
			{
				a.setClickable(false);
				b.setClickable(false);
				c.setClickable(false);
				d.setClickable(false);
				e.setClickable(false);
				f.setClickable(false);
				g.setClickable(false);
				h.setClickable(false);
				n.setClickable(false);
				final TextView v= (TextView)p1;
				v.setBackgroundColor(Color.WHITE);
				if(v.getTag().toString().equals("a")){
					a.setBackgroundColor(Color.YELLOW);
				}else if(v.getTag().toString().equals("b")){
					b.setBackgroundColor(Color.CYAN);
				}else if(v.getTag().toString().equals("c")){
					c.setBackgroundColor(Color.MAGENTA);
				}else if(v.getTag().toString().equals("d")){
					d.setBackgroundColor(Color.BLUE);
				}else if(v.getTag().toString().equals("e")){
					e.setBackgroundColor(Color.RED);
				}else if(v.getTag().toString().equals("f")){
					f.setBackgroundColor(Color.GREEN);
				}else if(v.getTag().toString().equals("g")){
					g.setBackgroundColor(Color.LTGRAY);
				}else if(v.getTag().toString().equals("h")){
					h.setBackgroundColor(Color.BLACK);
				}else if(v.getTag().toString().equals("n")){
					n.setBackgroundColor(Color.WHITE);
				}
				currentstring=currentstring+v.getTag().toString();
				if(!cmp()){
					v.setOnClickListener(reset);
					a.setClickable(false);
					b.setClickable(false);
					c.setClickable(false);
					d.setClickable(false);
					e.setClickable(false);
					f.setClickable(false);
					g.setClickable(false);
					h.setClickable(false);
					n.setClickable(false);
					v.setClickable(true);
					v.setText("Play Again");
					v.setGravity(Gravity.CENTER);
					v.setTextColor(Color.WHITE);
					v.setBackgroundColor(Color.BLACK);
					gameend();
				}else{
					if(hile){
					setTitle("Cheat Active! Score:"+score+" Max Score:"+sp.getInt("max",score));
					}else{
					setTitle("Score:"+score+" Max Score:"+sp.getInt("max",score));
					}if(!hile && gamesting.length()==currentstring.length()){
						gamenext();
						yak(gamesting,a,b,c,d,e,f,g,h,n);
					}
				h.postDelayed(new Runnable(){

						@Override
						public void run()
						{
							a.setBackgroundColor(Color.BLUE);
							b.setBackgroundColor(Color.RED);
							c.setBackgroundColor(Color.GREEN);
							d.setBackgroundColor(Color.YELLOW);
							e.setBackgroundColor(Color.CYAN);
							f.setBackgroundColor(Color.MAGENTA);
							g.setBackgroundColor(Color.DKGRAY);
							h.setBackgroundColor(Color.WHITE);
							n.setBackgroundColor(Color.BLACK);
							a.setClickable(true);
							b.setClickable(true);
							c.setClickable(true);
							d.setClickable(true);
							e.setClickable(true);
							f.setClickable(true);
							g.setClickable(true);
							h.setClickable(true);
							n.setClickable(true);
						}
					}, 10);
				}
			}
		};
		a.setOnClickListener(ocl);
		b.setOnClickListener(ocl);
		c.setOnClickListener(ocl);
		d.setOnClickListener(ocl);
		e.setOnClickListener(ocl);
		f.setOnClickListener(ocl);
		g.setOnClickListener(ocl);
		h.setOnClickListener(ocl);
		n.setOnClickListener(ocl);
		super.onCreate(savedInstanceState);
        setContentView(main);
    }
	
	public void yak(String s,final TextView a,final TextView b,final TextView c,final TextView d,final TextView e,final TextView f,final TextView g,final TextView h,final TextView n){
		final String[] sarray = s.split("");
		a.setClickable(false);
		b.setClickable(false);
		c.setClickable(false);
		d.setClickable(false);
		e.setClickable(false);
		f.setClickable(false);
		g.setClickable(false);
		h.setClickable(false);
		n.setClickable(false);
		for(int j=1;j<sarray.length;j++){
			final int i=j;
			h.postDelayed(new Runnable(){
					@Override
					public void run()
					{		
						if(sarray[i].equals("a")){
							a.setBackgroundColor(Color.YELLOW);
						}else if(sarray[i].toString().equals("b")){
							b.setBackgroundColor(Color.CYAN);
						}else if(sarray[i].toString().equals("c")){
							c.setBackgroundColor(Color.MAGENTA);
						}else if(sarray[i].toString().equals("d")){
							d.setBackgroundColor(Color.BLUE);
						}else if(sarray[i].toString().equals("e")){
							e.setBackgroundColor(Color.RED);
						}else if(sarray[i].toString().equals("f")){
							f.setBackgroundColor(Color.GREEN);
						}else if(sarray[i].toString().equals("g")){
							g.setBackgroundColor(Color.LTGRAY);
						}else if(sarray[i].toString().equals("h")){
							h.setBackgroundColor(Color.BLACK);
						}else if(sarray[i].toString().equals("n")){
							n.setBackgroundColor(Color.WHITE);
						}
					}
				},300*(i+2));
			h.postDelayed(new Runnable(){

					@Override
					public void run()
					{		
						a.setBackgroundColor(Color.BLUE);
						b.setBackgroundColor(Color.RED);
						c.setBackgroundColor(Color.GREEN);
						d.setBackgroundColor(Color.YELLOW);
						e.setBackgroundColor(Color.CYAN);
						f.setBackgroundColor(Color.MAGENTA);
						g.setBackgroundColor(Color.DKGRAY);
						h.setBackgroundColor(Color.WHITE);
						n.setBackgroundColor(Color.BLACK);		
					}
				},300*(i+2)+(300-r.nextInt(300)));
		}
		a.setClickable(true);
		b.setClickable(true);
		c.setClickable(true);
		d.setClickable(true);
		e.setClickable(true);
		f.setClickable(true);
		g.setClickable(true);
		h.setClickable(true);
		n.setClickable(true);
		}
		public boolean cmp(){
			try{
			if(hile){
				lvl=lvl+1;
			}
			score=score+lvl;
			SharedPreferences sp = getSharedPreferences("score",MODE_PRIVATE);
			SharedPreferences.Editor spe =sp.edit();
			if(!hile && sp.getInt("max",score)<=score){
				spe.putInt("max",score);
				spe.commit();
			}
			if(currentstring.length()>0){
			return hile || gamesting.substring(0,currentstring.length()).equals(currentstring);
			}else{
				return true;
			}
			}catch(Exception e){
				return true;
			}
		}
		public void gameend(){
			SharedPreferences sp = getSharedPreferences("score",MODE_PRIVATE);
			setTitle("Game Over... Score:"+score+" Max Score:"+sp.getInt("max",score));
			score=0;
			lvl=1;
			gamesting="";
			currentstring="";
		}
	public void gamenext(){
			String[] tabs={"a","b","c","d","e","f","g","h","n"};
			currentstring="";	
			gamesting=gamesting+tabs[r.nextInt(tabs.length)];
			lvl=lvl+1;
		}
}
